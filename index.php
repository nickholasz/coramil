<?php
$height = $_GET['height'];
$weight = $_GET['weight'];
$heightInMs = $height/100;
$bmi = $weight/($heightInMs*$heightInMs);
$message = "";

if($bmi >= 25.0){
    $message = "overweight";
}
else if($bmi >= 18.5 && $bmi <= 24.9){
    $message = "normal";
}
else{
    $message = "underweight";
}
$output = array(
    "bmi"=>round($bmi,1),
    "label"=>$message
);
header('Content-Type: application/json');
echo json_encode($output, JSON_PRETTY_PRINT);
?>